﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using SB.Core.ViewModels;
using UIKit;

namespace TipCalc.UI.iOS.Views
{
    public partial class TipView : MvxViewController<ViewModel>
    {
        public TipView() : base(nameof(TipView), null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.AddGestureRecognizer(new UITapGestureRecognizer(() =>
            {
                
            }));
        }
    }
}