using Android.App;
using Android.OS;
using MvvmCross.Platforms.Android.Views;
using SB.Core.ViewModels;

namespace TipCalc.UI.Droid.Views
{
    [Activity(Label = "Test", MainLauncher = true)]
    public class MyView : MvxActivity<ViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.View);
        }
    }
}