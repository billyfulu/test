﻿using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using SB.Core.Services;
using SB.Core.ViewModels;

namespace SB.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
//            CreatableTypes()
//                .EndingWith("Service")
//                .AsInterfaces()
//                .RegisterAsLazySingleton();

            Mvx.IoCProvider.ConstructAndRegisterSingleton<IService, MyService>();

            RegisterAppStart<ViewModel>();
        }
    }
}
